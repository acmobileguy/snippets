import React from 'react';
import { withAuth } from '@okta/okta-react';
import Moment from 'react-moment';
import defaultProfilePic from '../../img/default-profile-icon.jpg';

Moment.globalFormat = 'D MMM YYYY';

export default withAuth(
   class ProfilePage extends React.Component {
      constructor(props) {
         super(props);
         this.state = { user: null };
         this.getCurrentUser = this.getCurrentUser.bind(this);
      }

      async getCurrentUser() {
         this.props.auth.getUser().then(user => this.setState({ user }));
      }

      componentDidMount() {
         this.getCurrentUser();
      }

      render() {
         if (!this.state.user) return null;
         return (
            <div className="container">
               <div class="col-sm-12 mx-auto">
                  <div className="card">
                     <div className="card-header">
                        <h3>User Profile</h3>
                     </div>
                     <div className="card-body">
                        <div class="form-group row">
                           <div id="profile-wrapper" className="col-sm-12 col-md-3">
                              <div id="profile-pic-wrapper" className="col-6 offset-3 col-sm-6 offset-sm-3 col-md-12 offset-md-0 d-flex justify-content-center align-items-center">
                              <img src={defaultProfilePic} id="profile-pic" className="rounded-circle img-fluid" alt="Profile Image" />
                              </div>
                           </div>
                           <div id="profile-info-wrapper" className="col-sm-12 col-md-9">
                              <div id="profile-info">
                                 <div class="form-group row">
                                    <label>Name:</label>&nbsp;<span className="form-control-static">{this.state.user.name}</span>
                                 </div>
                                 <div class="form-group row">
                                    <label>Email:</label>&nbsp;<span className="form-control-static">{this.state.user.email}</span>
                                 </div>
                                 <div class="form-group row">
                                    <label>Timezone:</label>&nbsp;<span className="form-control-static">{this.state.user.zoneinfo}</span>
                                 </div>
                                 <div class="form-group row">
                                    <label>Last Updated:</label>&nbsp;<span className="form-control-static"><Moment unix>{this.state.user.updated_at}</Moment></span>
                                 </div>                              
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         );
      }
   }
);