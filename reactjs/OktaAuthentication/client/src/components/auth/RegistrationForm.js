import React from 'react';
import OktaAuth from '@okta/okta-auth-js';
import { withAuth } from '@okta/okta-react';

import config from '../../app.config';

export default withAuth(
  class RegistrationForm extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        firstName: '',
        lastName: '',
        email: '',
        password: '',
        sessionToken: null
      };
      this.oktaAuth = new OktaAuth({ url: config.url });
      this.checkAuthentication = this.checkAuthentication.bind(this);
      this.checkAuthentication();

      this.handleSubmit = this.handleSubmit.bind(this);
      this.handleFirstNameChange = this.handleFirstNameChange.bind(this);
      this.handleLastNameChange = this.handleLastNameChange.bind(this);
      this.handleEmailChange = this.handleEmailChange.bind(this);
      this.handlePasswordChange = this.handlePasswordChange.bind(this);
    }

    async checkAuthentication() {
      const sessionToken = await this.props.auth.getIdToken();
      if (sessionToken) {
        this.setState({ sessionToken });
      }
    }

    componentDidUpdate() {
      this.checkAuthentication();
    }

    handleFirstNameChange(e) {
      this.setState({ firstName: e.target.value });
    }
    handleLastNameChange(e) {
      this.setState({ lastName: e.target.value });
    }
    handleEmailChange(e) {
      this.setState({ email: e.target.value });
    }
    handlePasswordChange(e) {
      this.setState({ password: e.target.value });
    }

    handleSubmit(e) {
      e.preventDefault();
      fetch('/api/users', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(this.state)
      })
        .then(user => {
          this.oktaAuth
            .signIn({
              username: this.state.email,
              password: this.state.password
            })
            .then(res =>
              this.setState({
                sessionToken: res.sessionToken
              })
            );
        })
        .catch(err => console.log);
    }

    render() {
      if (this.state.sessionToken) {
        this.props.auth.redirect({ sessionToken: this.state.sessionToken });
        return null;
      }

      return (

         <div className="container">
            <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
               <div className="card">
                  <div className="card-header">
                     <h3>Register</h3>
                  </div>
                  <div className="card-body">
                     <form onSubmit={this.handleSubmit}>
                        <div className="form-row">
                           <div className="form-group col-md-6">
                              <label for="firstName">First Name:</label>
                              <input
                                 className="form-control"
                                 type="text"
                                 id="firstName"
                                 placeholder="First Name"
                                 value={this.state.firstName}
                                 onChange={this.handleFirstNameChange} />
                           </div>
                           <div className="form-group col-md-6">
                              <label for="lastName">Last Name:</label>
                              <input
                                 className="form-control"
                                 type="text"
                                 id="lastName"
                                 placeholder="Last Name"
                                 value={this.state.lastName}
                                 onChange={this.handleLastNameChange} />
                           </div>
                        </div>
                        <div className="form-group">
                           <label for="email">Email:</label>
                           <input
                              className="form-control"
                              type="email"
                              id="email"
                              placeholder="E-mail"
                              value={this.state.email}
                              onChange={this.handleEmailChange} />
                        </div>
                        <div className="form-group">
                           <label for="password">Password:</label>
                           <input
                              className="form-control"
                              type="password"
                              id="password"
                              placeholder="Password"
                              value={this.state.password}
                              onChange={this.handlePasswordChange} />
                        </div>
                        <input id="registerUser" type="submit" value="Register" class="btn float-right register_btn" />
                     </form>
                  </div>
               </div>
            </div>
         </div>


      );
    }
  }
);