import React from 'react';
import OktaAuth from '@okta/okta-auth-js';
import { withAuth } from '@okta/okta-react';
import { IoIosPerson, IoIosKey } from 'react-icons/io';

export default withAuth(
    class LoginForm extends React.Component {
        constructor(props) {
            super(props);
            this.state = {
                sessionToken: null,
                error: null,
                username: '',
                password: ''
            };

            this.oktaAuth = new OktaAuth({ url: props.baseUrl });

            this.handleSubmit = this.handleSubmit.bind(this);
            this.handleUsernameChange = this.handleUsernameChange.bind(this);
            this.handlePasswordChange = this.handlePasswordChange.bind(this);
        }

        handleSubmit(e) {
            e.preventDefault();
            this.oktaAuth
                .signIn({
                    username: this.state.username,
                    password: this.state.password
                })
                .then(res =>
                    this.setState({
                        sessionToken: res.sessionToken
                    })
                )
                .catch(err => {
                    this.setState({ error: err.message });
                    console.log(err.statusCode + ' error', err);
                });
        }

        handleUsernameChange(e) {
            this.setState({ username: e.target.value });
        }

        handlePasswordChange(e) {
            this.setState({ password: e.target.value });
        }

        render() {
            if (this.state.sessionToken) {
                this.props.auth.redirect({ sessionToken: this.state.sessionToken });
                return null;
            }

            const errorMessage = this.state.error ? (
                <span className="error-message">{this.state.error}</span>
            ) : null;
            
            return (
                <div className="container">
                    <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
                        <div className="card">
                            <div className="card-header">
                                <h3>Sign In</h3>
                            </div>
                            <div className="card-body">
                                <form className="form-signin" onSubmit={this.handleSubmit}>
                                    {errorMessage}
                                    <div class="input-group form-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><IoIosPerson /></span>
                                        </div>
                                        <input 
                                            id="username" 
                                            type="text" 
                                            className="form-control" 
                                            placeholder="username"
                                            value={this.state.username}
                                            onChange={this.handleUsernameChange} />
                                    </div>
                                    <div className="input-group form-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><IoIosKey /></span>
                                        </div>
                                        <input
                                            id="password"
                                            type="password"
                                            className="form-control" 
                                            placeholder="password" 
                                            value={this.state.password}
                                            onChange={this.handlePasswordChange}
                                        />
                                    </div>
                                    <div class="form-group">
                                    <input id="loginUser" type="submit" value="Login" class="btn float-right login_btn" />
					                </div>
                                </form>
                            </div>
                            <div class="card-footer">
                                <div class="d-flex justify-content-center links">
                                    Don't have an account?&nbsp;<a href="#">Sign Up</a>
                                </div>
                                <div class="d-flex justify-content-center">
                                    <a href="#">Forgot your password?</a>
                                </div>
                            </div>                            
                        </div>
                    </div>
                </div>
            );
        }
    }
);