import React from 'react';
import { Link } from 'react-router-dom';
import { withAuth } from '@okta/okta-react';
import { IoIosLogIn, IoIosLogOut, IoMdCreate, IoIosContact } from 'react-icons/io';

export default withAuth(
  class Navigation extends React.Component {
    constructor(props) {
      super(props);
      this.state = { authenticated: null };
      this.checkAuthentication = this.checkAuthentication.bind(this);
      this.checkAuthentication();
    }

    async checkAuthentication() {
      const authenticated = await this.props.auth.isAuthenticated();
      if (authenticated !== this.state.authenticated) {
        this.setState({ authenticated });
      }
    }

    componentDidUpdate() {
      this.checkAuthentication();
    }

    render() {
      if (this.state.authenticated === null) return null;
      const authNav = this.state.authenticated ? (
        <div className="auth-nav navbar-nav">
          <Link to="/login" className="nav-link" onClick={() => this.props.auth.logout()}><IoIosLogOut /> Logout</Link>
          <Link to="/profile" className="nav-link"><IoIosContact /> Profile</Link>
        </div>
      ) : (
        <div className="auth-nav navbar-nav">
          <Link to="/login" className="nav-link" onClick={() => this.props.auth.login()}><IoIosLogIn /> Login</Link>
          <Link to="/register" className="nav-link"><IoMdCreate /> Register</Link>
        </div>
      );
      return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
          <Link to={'/'} className="navbar-brand">Team Achieve</Link>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#teamNavigation" aria-controls="teamNavigation" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>

          <div className="collapse navbar-collapse justify-content-between" id="teamNavigation">
            <div className="navbar-nav">
            <Link to="/" className="nav-item nav-link">Home</Link>
            </div>
            {authNav}
          </div>
        </nav>
      );
    }
  }
);