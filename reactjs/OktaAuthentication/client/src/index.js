import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import { Security } from '@okta/okta-react';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import appConfig from './app.config';

function onAuthRequired({ history }) {
    history.push('/login');
}

ReactDOM.render(
    <Router>
        <Security 
            issuer={appConfig.issuer}
            client_id={appConfig.client_id}
            redirect_uri={appConfig.redirect_uri}
            onAuthRequired={onAuthRequired}
        >
            <App />
        </Security>
    </Router>, document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
